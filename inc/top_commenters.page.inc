<?php

/**
 * @file
 * Inc file for /top-commenters menu item.
 */

/**
 * Implements top_commenters_list_page().
 */
function top_commenters_list_page() {
  // Include css file.
  drupal_add_css(drupal_get_path('module', 'top_commenters') . '/css/style.css', array('group' => CSS_THEME, 'every_page' => FALSE));
  return theme('top_commenters_page');
}
