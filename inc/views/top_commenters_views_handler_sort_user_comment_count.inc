<?php

/**
 * @file
 * Contains the Comment count sort handler.
 */

/**
 * Views sort user comment handler to display some configurable result summary.
 *
 * @ingroup views_handler_sort
 */
class top_commenters_views_handler_sort_user_comment_count extends views_handler_sort {
/**
 * Add order to user comment count.
 */
  function query() {
    $this->ensure_my_table();
    $this->query->add_orderby(NULL, NULL, $this->options['order'], 'user_comment_count');
  }
}
