<?php

/**
 * @file
 * Provides support for the Views module.
 */

/**
 * Implements hook_views_data().
 */
function top_commenters_views_data() {
  return array(
    'views' => array(
      'user_comment_count' => array(
        'title' => t('Comment count'),
        'help' => t('Number of user comments.'),
        'group' => t('User'),
        'field'  => array(
          'handler' => 'top_commenters_views_handler_field_user_comment_count',
        ),
        'sort' => array(
          'handler' => 'top_commenters_views_handler_sort_user_comment_count',
        ),
      ),
    ),
  );
}
