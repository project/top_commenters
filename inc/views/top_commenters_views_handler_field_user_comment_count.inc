<?php

/**
 * @file
 * Contains the Comment count numeric field handler.
 */
/**
 * Views user comment count handler to display some configurable result summary.
 *
 * @ingroup views_handler_field_numeric
 */
class top_commenters_views_handler_field_user_comment_count extends views_handler_field_numeric {
/**
  * Find out the information to render count.
  */
  function query() {
    $this->ensure_my_table();
    $this->field_alias = $this->query->add_field(
      NULL,
      '(SELECT COUNT(*) FROM {comment} c WHERE c.uid = users.uid AND c.status = 1)',
      'user_comment_count'
    );
   $this->add_additional_fields();
  }
}
