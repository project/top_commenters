<?php

/**
 * @file
 * Provide Top Commenters backend setting form.
 */

/**
 * Function to generate the backend form.
 */
function top_commenters_admin_settings() {
  $filter_options = array(
    0 => t('Current Week'),
    1 => t('Fortnight'),
    2 => t('Month'),
    3 => t('Other'),
  );
  $form['tc_basic_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Basic Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['tc_basic_settings']['top_commenters_count'] = array(
    '#title' => t('Number of users to list'),
    '#type' => 'textfield',
    '#description' => t('Enter number of top commenters to be listed.'),
    '#default_value' => variable_get('top_commenters_count', 3),
  );
  $form['tc_basic_settings']['top_commenters_order_as'] = array(
    '#title' => t('List Order'),
    '#type' => 'select',
    '#description' => t('Select the order for top commenters list.'),
    '#options' => array('ASC' => t('Ascending'), 'DESC' => t('Descending')),
    '#default_value' => variable_get('top_commenters_order_as', array('DESC')),
  );
  $form['tc_img'] = array(
    '#type' => 'fieldset',
    '#title' => t('User Picture'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['tc_img']['top_commenters_img_account'] = array(
    '#type' => 'checkbox',
    '#title' => t('From User Account'),
    '#description' => t('Use picture from user account.'),
    '#default_value' => variable_get('top_commenters_img_account', FALSE),
  );
  $form['tc_img']['top_commenters_img_style'] = array(
    '#type' => 'select',
    '#title' => t('Image Style'),
    '#description' => t("Choose image style to be used for commenter's picture."),
    '#default_value'  => variable_get('top_commenters_img_style', array()),
    '#options' => image_style_options(),
    '#states' => array(
      // Hide the textfield when the user picture checkbox is disabled.
      'invisible' => array(
        ':input[name="top_commenters_img_account"]' => array('checked' => FALSE),
      ),
    ),
  );
  $form['tc_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content Types'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['tc_types']['top_commenters_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Choose Content types'),
    '#description' => t('Mark content types for which top commenters should be listed.'),
    '#default_value'  => variable_get('top_commenters_node_types', array()),
    '#options' => node_type_get_names(),
  );
  $form['tc_filter_commenters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter Commenters Listing'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['tc_filter_commenters']['top_commenters_predefined_filters'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Filters'),
    '#default_value'  => variable_get('top_commenters_predefined_filters', FALSE),
    '#description' => t('Mark this to use filters to filter the top commenters list.'),
  );
  $form['tc_filter_commenters']['top_commenters_predefined_filters_options'] = array(
    '#type' => 'select',
    '#title' => t('Filter By'),
    '#default_value'  => variable_get('top_commenters_predefined_filters_options', array()),
    '#options' => $filter_options,
    '#states' => array(
      // Display this field when the predefined_filters checkbox is enabled.
      'visible' => array(
        ':input[name="top_commenters_predefined_filters"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['tc_filter_commenters']['top_commenters_filter_from_date'] = array(
    '#type' => 'date',
    '#title' => t('Start date'),
    '#description' => t('Specify start date for top commenters filtering.'),
    '#default_value'  => variable_get('top_commenters_filter_from_date', array()),
    '#states' => array(
      // Display this field Use Filters has 'other'.
      'visible' => array(
        ':input[name="top_commenters_predefined_filters"]' => array('checked' => TRUE),
        ':input[name="top_commenters_predefined_filters_options"]' => array('value' => 3),
      ),
    ),
    '#required' => TRUE,
  );
  $form['tc_filter_commenters']['top_commenters_filter_to_date'] = array(
    '#type' => 'date',
    '#title' => t('End date'),
    '#description' => t('Specify end date for top commenters filtering.'),
    '#default_value'  => variable_get('top_commenters_filter_to_date', array()),
    '#states' => array(
      // Display this field Use Filters has 'other'.
      'visible' => array(
        ':input[name="top_commenters_predefined_filters"]' => array('checked' => TRUE),
        ':input[name="top_commenters_predefined_filters_options"]' => array('value' => 3),
      ),
    ),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}
