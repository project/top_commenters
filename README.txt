CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Author/Maintainers

INRODUCTION
-----------

 * This module list the users who have posted comments on nodes.

REQUIREMENTS
------------
 
 * Views(http://drupal.org/project/views) module.

INSTALLATION
------------
 
 * To install, copy the top_commenters directory and all its contents to
   your sites/all/modules directory. 
 
 * To enable the module go to Administer > Modules, and enable "Top Commenters".

CONFIGURATION
-------------
 
 * Configure module settings in Administration » Config » People
   » Top Commenters Settings(admin/config/people/top-commenters).

 * Enter number of top commenters to be listed on the /top-commenters page
   and Top Commenters block.

 * Define the order of list by default is is "Descending" order.

 * If you want user pictures also in the list alogn with name check the
   "From User Account" checkbox and define the image style to be used.

 * Choose the content types for which commenters should be listed
   if no content types selected commenters will be listed
   for all the content types.

 * If you want to filter the list depending upon different criteria check
   "Use Filters" and select "Current Week", "Fortnight", "Month" OR specify
   the date range in "Start Date" and "End Date" fields.

 * This module provides a block named "Top Commenters" as
   well as page /top-commenters.

 * Configure user permissions in Administration » People » Permissions:

  - Administer Top-Commenters

    Users with this permission will be able to configure the module.

  - View Top-Commenters Page

    Users with this permission will be able to access /top-commenters page.
 
 * Views Integration

  - This module also provides a "Comment Count" field under User group in Views.

AUTHOR/MAINTAINERS
------------------
Author:
 * Prashant Chauhan(Prashant.c) - https://www.drupal.org/user/1936756/
Current maintainers:
 * Sumit Kumar(Sumit kumar) - https://www.drupal.org/user/2445096/ 
