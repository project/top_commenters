<?php

/**
 * @file
 * Theme file to handle Top Commenters block display.
 */
?>
<?php if(isset($variables['tc_results'])): ?>
  <?php foreach($variables['tc_results'] as $key => $values): ?>
  <div class = 'top-commenter-wrapper'>
    <div class = 'top-commenter-sub-wrapper'>
      <?php if(isset($values['tc_use_image'])): ?>
      <span class = 'commenter-img'>
        <img src = <?php echo $values['tc_image']; ?> class = <?php echo $values['tc_image_class']; ?>>
      </span>
      <?php endif; ?>
      <div class = 'tc-name-comment'>
        <span><?php echo $values['tc_username']; ?></span>
        <span><?php echo $values['tc_count_text']; ?></span>
      </div>
    </div>
  </div>
<?php
  endforeach;
endif;
?>
